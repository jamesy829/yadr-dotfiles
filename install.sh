#!/bin/bash

if [ ! -d "$HOME/.yadr" ]; then
  echo "Installing YADR for the first time"

  if [[ "$OSTYPE" == linux* ]]; then
    echo  "======================================================"
    echo  "Add git-core ppa and install base ruby and rake."
    echo  "======================================================"
    sudo add-apt-repository -y universe
    sudo add-apt-repository -y ppa:git-core/ppa
    sudo apt-get update
    sudo apt-get install -y -f git curl wget aptitude build-essential software-properties-common
    sudo apt-get install -y -f zlib1g-dev libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev libffi-dev
    sudo apt-get install -y -f ruby ruby-dev libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
    sudo gem install rake
  fi

  git clone git@bitbucket.org:jamesy829/yadr-dotfiles.git "$HOME/.yadr"
  cd "$HOME/.yadr"
  [ "$1" = "ask" ] && export ASK="true"
  rake install

  if [[ "$OSTYPE" == linux* ]]; then
    echo  "======================================================"
    echo  "Cleaning APT."
    echo  "======================================================"
    sudo apt-get -y purge ruby ruby-dev
    sudo apt-get -y autoremove
    sudo apt-get -y autoclean
  fi
else
  echo "YADR is already installed"
fi
