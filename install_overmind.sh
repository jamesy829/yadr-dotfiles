#!/bin/bash --login

cd ~
pwd

wget https://github.com/DarthSim/overmind/releases/download/v2.2.0/overmind-v2.2.0-linux-amd64.gz
gunzip overmind-v2.2.0-linux-amd64.gz
mv overmind-v2.2.0-linux-amd64 overmind
chmod +x overmind
sudo mv overmind /usr/local/bin
