# Set nvim as default editor and visual
if is-callable 'nvim' ; then
  export EDITOR="nvim"
  export VISUAL='nvim'
else
  export EDITOR="vim"
  export VISUAL='vim'
fi

if [[ "$OSTYPE" == darwin* ]]; then
  export LANG=en_US.UTF-8
  export LC_COLLATE=C

  # Brew
  eval "$(/opt/homebrew/bin/brew shellenv)"

  # Fish-shell-like
  source $(brew --prefix zsh-autosuggestions)/share/zsh-autosuggestions/zsh-autosuggestions.zsh
  # source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

  # NVM
  export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion

  # NVM Shell Integration
  if [ -e "/usr/local/opt/nvm/nvm.sh" ]; then
    autoload -U add-zsh-hook
    load-nvmrc() {
      local node_version="$(nvm version)"
      local nvmrc_path="$(nvm_find_nvmrc)"

      if [ -n "$nvmrc_path" ]; then
        local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

        if [ "$nvmrc_node_version" = "N/A" ]; then
          nvm install
        elif [ "$nvmrc_node_version" != "$node_version" ]; then
          nvm use
        fi
      elif [ "$node_version" != "$(nvm version default)" ]; then
        echo "Reverting to nvm default version"
        nvm use default
      fi
    }
    add-zsh-hook chpwd load-nvmrc
    load-nvmrc
  fi

  # Go
  export GOPATH="${HOME}/.go"
  export GOROOT="$(brew --prefix golang)/libexec"
  export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"

  # Google Cloud Platform
  # The next line updates PATH for the Google Cloud SDK.
  # source '/opt/homebrew/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc'

  # The next line enables shell command completion for gcloud.
  # source '/opt/homebrew/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc'

  # Heroku
  if type brew &>/dev/null
  then
    FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

    autoload -Uz compinit
    compinit
  fi
fi

# Load Linux specific config
if [[ "$OSTYPE" == linux* ]]; then
  export LC_COLLATE=C

  # Terminal color
  # if which tput > /dev/null 2>&1 && [[ $(tput -T$TERM colors) -ge 17 ]]; then
    export TERM=screen-256color       # for a tmux -2 session (also for screen)
  # elif which tput > /dev/null 2>&1 && [[ $(tput -T$TERM colors) -ge 9 ]]; then
  # else
    # export TERM=xterm-16color
    # export TERM=xterm
  # fi
  # export TERM=xterm-256color        # for common 256 color terminals (e.g. gnome-terminal)
  # export TERM=rxvt-unicode-256color # for a colorful rxvt unicode session

  # Fish-shell-like
  source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

  # Directory color
  if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  fi

  # SSH Agents
  if [ -e ~/ssh-find-agent.sh ]; then
    source ~/ssh-find-agent.sh

    ssh-find-agent -a
    if [ -z "$SSH_AUTH_SOCK" ]
    then
      eval $(ssh-agent) > /dev/null
      ssh-add -l >/dev/null || alias ssh="ssh-add -l >/dev/null || ssh-add && unalias ssh; ssh"
    fi
  fi

  # Hub
  if [ -e ~/.zsh/completions/_hub ]; then
    eval "$(hub alias -s)"
    fpath=(~/.zsh/completions $fpath)
    autoload -Uz compinit && compinit
  fi

  # Google Cloud Platform
  # The next line updates PATH for the Google Cloud SDK.
  if [ -f "$HOME/google-cloud-sdk/path.zsh.inc" ]; then source "$HOME/google-cloud-sdk/path.zsh.inc"; fi

  # The next line enables shell command completion for gcloud.
  if [ -f "$HOME/google-cloud-sdk/completion.zsh.inc" ]; then source "$HOME/google-cloud-sdk/completion.zsh.inc"; fi

  # NVM
  if [ -e ~/.nvm/nvm.sh ]; then
    export NVM_DIR="$HOME/.nvm"
    [[ -s "$NVM_DIR/nvm.sh" ]] && source "$NVM_DIR/nvm.sh" # This loads nvm
  fi

  # NVM Shell Integration
  if [ -e "$NVM_DIR/nvm.sh" ]; then
    autoload -U add-zsh-hook
    load-nvmrc() {
      local node_version="$(nvm version)"
      local nvmrc_path="$(nvm_find_nvmrc)"

      if [ -n "$nvmrc_path" ]; then
        local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

        if [ "$nvmrc_node_version" = "N/A" ]; then
          nvm install
        elif [ "$nvmrc_node_version" != "$node_version" ]; then
          nvm use
        fi
      elif [ "$node_version" != "$(nvm version default)" ]; then
        echo "Reverting to nvm default version"
        nvm use default
      fi
    }
    add-zsh-hook chpwd load-nvmrc
    load-nvmrc
  fi

  # PIP
  export PATH=~/.local/bin:$PATH
  # Poetry
  export PATH=$HOME/.poetry/bin:$PATH

  # Go
  export GOPATH="${HOME}/.go"
  export GOROOT=/usr/local/go
  export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"

fi

# PSQL
export PAGER=less
export LESS="-iMSx4 -FXRS -e"

# Android
export ANDROID_HOME=${HOME}/Android/Sdk
export PATH=${PATH}:${ANDROID_HOME}/tools
export PATH=${PATH}:${ANDROID_HOME}/platform-tools

# Kubectl
if [ $commands[kubectl] ]; then
  source <(kubectl completion zsh)
fi

# terraform
autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /opt/homebrew/bin/terraform terraform

# AVN
# [[ -s "$HOME/.avn/bin/avn.sh" ]] && source "$HOME/.avn/bin/avn.sh" # load avn

# FZF
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --no-ignore-vcs -g "!{node_modules,.git,coverage}"'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# RVM
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"
# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
