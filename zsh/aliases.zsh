# Aliases in this file are bash and zsh compatible

# Don't change. The following determines where YADR is installed.
yadr=$HOME/.yadr

# Get operating system
platform='unknown'
unamestr=$(uname)
if [[ $unamestr == 'Linux' ]]; then
  platform='linux'
elif [[ $unamestr == 'Darwin' ]]; then
  platform='darwin'
fi

# PS
alias psa="ps aux"
alias psg="ps aux | grep "
alias psr='ps aux | grep ruby'

# Moving around
alias cdb='cd -'
alias cls='clear;ls'

# Show human friendly numbers and colors
alias df='df -h'
alias du='du -h -d 2'

if [[ $platform == 'linux' ]]; then
  alias ll='ls -alh --color=auto'
  alias ls='ls -F --color=auto'
  alias grep='grep --color=auto'
elif [[ $platform == 'darwin' ]]; then
  alias ll='ls -alGh'
  alias ls='ls -Gh'
fi

# show me files matching "ls grep"
alias lsg='ll | grep'

# Alias Editing
TRAPHUP() {
  source $yadr/zsh/aliases.zsh
}
alias ae='vim $yadr/zsh/aliases.zsh' #alias edit
alias ar='source $yadr/zsh/aliases.zsh'  #alias reload
alias gar="killall -HUP -u \"$USER\" zsh"  #global alias reload

# nvim using
alias vim="nvim"
alias v="nvim"
alias vi="nvim"
alias vf='nvim $(fzf)'

# mimic vim functions
alias :q='exit'

# vimrc editing
alias ve='vim ~/.vimrc'

# zsh profile editing
alias ze='vim ~/.zshrc'

# Git Aliases
alias gs='git status'
alias gstsh='git stash'
alias gst='git stash'
alias gsp='git stash pop'
alias gsa='git stash apply'
alias gsh='git show'
alias gshw='git show'
alias gshow='git show'
alias gi='vim .gitignore'
alias gcm='git ci -m'
alias gcim='git ci -m'
alias gci='git ci'
alias gco='git co'
alias gcp='git cp'
alias gcd="git checkout develop"
alias gcm="git checkout main"
alias ga='git add -A'
alias gap='git add -p'
alias guns='git unstage'
alias gunc='git uncommit'
alias gm='git merge'
alias gms='git merge --squash'
alias gam='git amend --reset-author'
alias grv='git remote -v'
alias grr='git remote rm'
alias grad='git remote add'
alias gr='git rebase'
alias gra='git rebase --abort'
alias ggrc='git rebase --continue'
alias gbi='git rebase --interactive'
alias gl='git l'
alias glg='git l'
alias glog='git l'
alias co='git co'
alias gf='git fetch'
alias gfp='git fetch --prune'
alias gfa='git fetch --all'
alias gfap='git fetch --all --prune'
alias gfch='git fetch'
alias gfu='gf --prune; gplr'
alias gd='git diff'
alias gb='git b'
# Staged and cached are the same thing
alias gdc='git diff --cached -w'
alias gds='git diff --staged -w'
alias gpub='grb publish'
alias gtr='grb track'
alias gpl='git pull'
alias gplr='git pull --rebase'
alias gps='git push'
alias gpsh='git push -u origin `git rev-parse --abbrev-ref HEAD`'
alias gnb='git nb' # new branch aka checkout -b
alias grs='git reset'
alias grsh='git reset --hard'
alias gcln='git clean'
alias gclndf='git clean -df'
alias gclndfx='git clean -dfx'
alias gsm='git submodule'
alias gsmi='git submodule init'
alias gsmu='git submodule update'
alias gt='git t'
alias gbg='git bisect good'
alias gbb='git bisect bad'
alias gdmb='git branch --merged | grep -v "\*" | xargs -n 1 git branch -d'
alias glatest='gst; gfu; gsp'

# Common shell functions
alias less='less -r'
alias tf='tail -f'
alias l='less'
alias lh='ls -alt | head' # see the last modified files
alias screen='TERM=screen screen'
alias cl='clear'

# Find and Replace
alias ruf="find . -name '*.un~' -exec rm -rf {} \;"

# Zippin
alias gz='tar -zcvf'

# Ruby
alias c='rails c' # Rails 3
alias co='script/console' # Rails 2
alias cod='script/console --debugger'

#If you want your thin to listen on a port for local VM development
#export VM_IP=10.0.0.1 <-- your vm ip
alias ts='thin start -a ${VM_IP:-127.0.0.1}'
alias fs='foreman start'
alias ms='mongrel_rails start'
alias tfdl='tail -f log/development.log'
alias tftl='tail -f log/test.log'

alias ka9='killall -9'
alias k9='kill -9'

# Gem install
alias sgi='sudo gem install --no-ri --no-rdoc'

# TODOS
# This uses NValt (NotationalVelocity alt fork) - http://brettterpstra.com/project/nvalt/
# to find the note called 'todo'
alias todo='open nvalt://find/todo'

# Forward port 80 to 3000
alias portforward='sudo ipfw add 1000 forward 127.0.0.1,3000 ip from any to any 80 in'

# Rails
alias srails='rails s'
alias rc='rails c'
alias dc='rails dbconsole'
alias rr='rake routes'
alias rt='rake -T'
alias rdm='rake db:migrate'
alias rdmr='rake db:migrate:redo'
alias rdmd='rake db:migrate:down'
alias rdmu='rake db:migrate:up'
alias rdr='rake db:rollback'


# Bundle
alias bi='bundle install'
alias brc='bundle exec rails c'
alias bdc='bundle exec rails dbconsole'
alias brr='bundle exec rake routes'
alias brt='bundle exec rake -T'
alias bd='bundle exec rake db:drop; bundle exec rake db:create; bundle exec rake db:migrate'
alias brdm='bundle exec rake db:migrate'
alias brdmr='bundle exec rake db:migrate:redo'
alias brdmd='bundle exec rake db:migrate:down'
alias brdmu='bundle exec rake db:migrate:up'
alias brdr='bundle exec rake db:rollback'

# Zeus
alias zs='zeus server'
alias zc='zeus console'
alias zr='zeus rspec'
alias zrc='zeus rails c'
alias zrs='zeus rails s'
alias zrdbm='zeus rake db:migrate'
alias zrdbtp='zeus rake db:test:prepare'
alias zzz='rm .zeus.sock; pkill zeus; zeus start'

# Rspec
alias rs='rspec spec'
alias sr='spring rspec'
alias src='spring rails c'
alias srgm='spring rails g migration'
alias srdm='spring rake db:migrate'
alias srdt='spring rake db:migrate'
alias srdmt='spring rake db:migrate db:test:prepare'

# Sprintly - https://github.com/nextbigsoundinc/Sprintly-GitHub
alias sp='sprintly'
# spb = sprintly branch - create a branch automatically based on the bug you're working on
alias spb="git checkout -b \`sp | tail -2 | grep '#' | sed 's/^ //' | sed 's/[^A-Za-z0-9 ]//g' | sed 's/ /-/g' | cut -d"-" -f1,2,3,4,5\`"

alias hpr='hub pull-request'
alias grb='git recent-branches'

# Finder
alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

alias dbtp='spring rake db:test:prepare'
alias dbm='spring rake db:migrate'
alias dbmr='spring rake db:migrate:redo'
alias dbmd='spring rake db:migrate:down'
alias dbmu='spring rake db:migrate:up'

# Homebrew
alias brewu='brew update && brew upgrade && brew cleanup && brew doctor'
alias outdatedCask="brew cask outdated --greedy --verbose | grep -v '(latest)' | cut -d' ' -f1"
alias brewcasku="brew update && brew cask upgrade ${outdatedCask} && brew cask cleanup && brew cask doctor"

# DockeR
alias dr='docker'
# DockeR Stop All
alias drsa='docker stop $(docker ps -a -q)'
# Docker Remove All Containers
alias drac='docker rm $(docker ps -a -q)'
# Docker Remove All Images
alias drai='docker rmi $(docker images -q)'

# Ubuntu
alias ubuntuUpdate='sudo aptitude update; sudo aptitude full-upgrade -y; sudo aptitude autoclean -y'
alias kvmUpdate='sudo iptables -P FORWARD ACCEPT; sudo iptables -L'
alias setCPUFreqPerformance='for ((i=0;i<$(nproc);i++)); do sudo cpufreq-set -c $i -r -g performance; done; cpufreq-info'

##########
# Custom #
##########

# Generate ctags
alias gctags="ctags -R --exclude=.git --exclude=.hg --exclude=.svn --exclude=.DS_Store --exclude=log --exclude=public --exclude=coverage --exclude=vendor --exclude=bower_components --exclude=node_modules --exclude=gulp --exclude=.vagrant --exclude=tmp *"

# Linux Command
alias reboot='sudo reboot'
alias arestart='sudo service apache2 restart'
alias nrestart='sudo service nginx restart'
alias prestart='touch tmp/restart.txt'
if [[ $platform == 'linux' ]]; then
  alias pbcopy='xclip -selection clipboard'
  alias pbpaste='xclip -selection clipboard -o'
elif [[ $platform == 'darwin' ]]; then
  alias pbcopy='pbcopy'
  alias pbpaste='pbpaste'
fi

# Heroku Alias
alias hr='heroku run'
alias hdm='hr bundle exec rake db:migrate'
alias hds='hr bundle exec rake db:seed'

# Tmux Alias
alias takeover="tmux detach -a"

# Mailcatcher
alias mc='mailcatcher'

# Docker & Kubernetes
alias deleteFailedPods="kubectl get pods --all-namespaces --field-selector 'status.phase==Failed' -o json | kubectl delete -f -"

function connectPodBy() {
  local pod=`kubectl get pods --field-selector=status.phase=Running --namespace $1 | grep $2 | awk '{print $1}' | head -1`

  kubectl exec $pod --namespace $1 -c $3 -it -- sh
}

function kubeConnectPod() {
  kubectl exec deployment/$2 --namespace $1 -c $3 -it -- sh
}

##########
# Personalyze #
##########

# Tunnel Alias
alias TopicdnaDbProxyOn='ssh -fN topicdna-db'
alias TopicdnaDbProxyCheck='ssh -O check topicdna-db'
alias TopicdnaDbProxyOff='ssh -O exit topicdna-db'
alias PersonalyzeDbProxyOn='ssh -fN personalyze-prod-db'
alias PersonalyzeDbProxyCheck='ssh -O check personalyze-prod-db'
alias PersonalyzeDbProxyOff='ssh -O exit personalyze-prod-db'
alias cgs="gcloud container clusters get-credentials personalyze-staging --zone europe-west1-b --project personalyze-188017 && gcloud config set project personalyze-188017"
alias cgp="gcloud container clusters get-credentials personalyze-prod --zone europe-west1-b --project personalyze-production && gcloud config set project personalyze-production"
alias uTDNA="cd ~/code/topicdna/topicdna-backend && gcd && cd ~/code/topicdna/topicdna-billing && gcd && cd ~/code/topicdna/topicdna-core && gcd && cd ~/code/topicdna/topicdna-emails && gcm && cd ~/code/topicdna/topicdna-frontend && gcd && cd ~/code/topicdna/topicdna-microservices && gcd && cd ~/code/topicdna/topicdna-reports && gcd && cd ~/code/topicdna/topicdna-tests && gcd && cd ~/code/topicdna"
alias css="gcloud config set project personalyze-188017 && cd ~/code && ./cloud_sql_proxy -instances=personalyze-188017:europe-west1:topicdna-11-staging=tcp:5436,personalyze-188017:us-west1:topicdna-sandbox=tcp:5433,personalyze-188017:europe-west1:entity-attributes-11-staging=tcp:5434,personalyze-188017:europe-west1:microservices-11-staging=tcp:5435"
alias csp="gcloud config set project personalyze-production && cd ~/code && ./cloud_sql_proxy -instances=personalyze-production:europe-west1:topicdna-11-prod=tcp:5536,personalyze-production:us-west1:topicdna-production=tcp:5533,personalyze-production:europe-west1:entity-attributes-11-prod=tcp:5537,personalyze-production:europe-west1:microservices-11-prod=tcp:5538"
alias entityStorage="kubectl port-forward deployment/ms-entitystorage --namespace=microservices 9004:9004"
alias fbAdManager="kubectl port-forward deployment/ms-admanager-facebook --namespace=microservices 9019:9019"
alias twAdManager="kubectl port-forward deployment/ms-admanager-twitter --namespace=microservices 9022:9022"
alias scAdManager="kubectl port-forward deployment/ms-admanager-snapchat --namespace=microservices 9021:9021"
alias lkAdManager="kubectl port-forward deployment/ms-admanager-linkedin --namespace=microservices 9020:9020"

##########
# GoBolt #
##########

# Tunnel Alias
alias chToStaging="gcloud config set project sc-staging-340315 && gcloud container clusters get-credentials basic-stg-shared --region northamerica-northeast2 --project sc-staging-340315"
alias chToPreProd="gcloud config set project sc-preprod && gcloud container clusters get-credentials prd-preprod --region northamerica-northeast2 --project sc-preprod"
alias chToProd="gcloud config set project sc-prod-341803 && gcloud container clusters get-credentials prd-prod --region northamerica-northeast2 --project sc-prod-341803"
