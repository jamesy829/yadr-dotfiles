require 'rake'
require 'fileutils'

$is_macos = RUBY_PLATFORM.downcase.include?("darwin")
$is_linux = RUBY_PLATFORM.downcase.include?("linux")

desc "Hook our dotfiles into system-standard positions."
task install: [:submodule_init, :submodules] do
  puts
  puts "======================================================"
  puts "Welcome to YADR Installation."
  puts "======================================================"
  puts

  install_homebrew if $is_macos
  install_linux_libraries if $is_linux

  Rake::Task["install_rvm"].execute
  Rake::Task["install_nvm"].execute
  Rake::Task["install_docker"].execute if $is_linux
  Rake::Task["install_go"].execute if $is_linux
  # Rake::Task["install_code_climate"].execute if $is_linux
  # Rake::Task["install_anycable_go"].execute if $is_linux
  # Rake::Task["install_overmind"].execute if $is_linux
  Rake::Task["install_deb_get"].execute if $is_linux
  Rake::Task["install_bat"].execute if $is_linux
  Rake::Task["install_popeye"].execute if $is_linux
  Rake::Task["install_helm"].execute if $is_linux
  Rake::Task["install_terraform"].execute if $is_linux
  install_rvm_binstubs

  # this has all the runcoms from this directory.
  install_files(Dir.glob('psql/*')) if want_to_install?('psqlrc')
  install_files(Dir.glob('git/*')) if want_to_install?('git configs (color, aliases)')
  install_files(Dir.glob('irb/*')) if want_to_install?('irb/pry configs (more colorful)')
  install_files(Dir.glob('ruby/*')) if want_to_install?('rubygems config (faster/no docs)')
  install_files(Dir.glob('javascript/*')) if want_to_install?('eslintrc shintrc jscsrc')
  install_files(Dir.glob('ctags/*')) if want_to_install?('ctags config (better js/ruby support)')
  install_files(Dir.glob('tmux/*')) if want_to_install?('tmux config')
  install_files(Dir.glob('vimify/*')) if want_to_install?('vimification of command line tools')
  if want_to_install?('vim configuration (highly recommended)')
    install_files(Dir.glob('{vim,vimrc}'))
  end
  install_files(Dir.glob('zsh/zshrc'))

  run %{ ln -nfs ~/.yadr/nvim-kickstart ~/.config/nvim }

  run %{ touch ~/.hushlogin }

  Rake::Task["install_prezto"].execute

  install_fonts

  if $is_macos
    install_term_theme
    run %{ ~/.yadr/iTerm2/bootstrap-iterm2.sh }
  end

  if $is_linux
    install_directory_color_fix
  end

  install_ssh_tunnel

  run_bundle_config

  success_msg("installed")
end

task :uninstall_yadr do
  run %{ sudo rm -rf $HOME/.mkshrc $HOME/.zshrc $HOME/.zshenv $HOME/.zsh.* $HOME/.zprofile $HOME/.zpreztorc \
         $HOME/.zprezto $HOME/.zlogin $HOME/.zlogout $HOME/.zcompdump $HOME/.zcompdump.zwc $HOME/.vimrc $HOME/.vim \
         $HOME/.unescaped_colors.rb $HOME/.tmux.conf $HOME/.rdebugrc $HOME/.pryrc $HOME/.jshintrc $HOME/.jscsrc \
         $HOME/.inputrc $HOME/.gitignore $HOME/.gitconfig $HOME/.gemrc $HOME/.fasd $HOME/.fasd-init-bash \
         $HOME/.eslintrc $HOME/.escaped_colors.rb $HOME/.editrc $HOME/.ctags $HOME/.aprc $HOME/.cache $HOME/.rvmrc \
         $HOME/.dircolors }
  run %{ cd $HOME }
  run %{ $HOME/.yadr }

  if $is_macos
    run %{ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)" }
    run %{ sudo chmod 0755 /usr/local }
    run %{ sudo chgrp wheel /usr/local }
    run %{ sudo rm -rf /Library/Caches/Homebrew }
  end
end

task :install_prezto do
  if want_to_install?('zsh enhancements & prezto')
    install_prezto
  end
end

desc 'Updates the installation'
task :update do
  Rake::Task["install"].execute
  #TODO: for now, we do the same as install. But it would be nice
  #not to clobber zsh files
end

task :submodule_init do
  unless ENV["SKIP_SUBMODULES"]
    run %{ git submodule update --init --recursive }
  end
end

desc "Init and update submodules."
task :submodules do
  unless ENV["SKIP_SUBMODULES"]
    puts "======================================================"
    puts "Downloading YADR submodules...please wait"
    puts "======================================================"

    run %{
      cd $HOME/.yadr
      git submodule update --recursive
      git clean -df
    }
    puts
  end
end

task :install_rvm do
  puts "======================================================"
  puts "Installing RVM"
  puts "======================================================"
  if want_to_install?('RVM')
    run %{ bash ./install_rvm.sh }
  end
end

task :install_nvm do
  puts "======================================================"
  puts "Installing NVM"
  puts "======================================================"
  if want_to_install?('NVM')
    run %{ bash ./install_nvm.sh }
  end
end

task :install_docker do
  puts "======================================================"
  puts "Installing Docker"
  puts "======================================================"
  if want_to_install?('Docker')
    run %{ bash ./install_docker.sh }
  end
end

task :install_go do
  puts "======================================================"
  puts "Installing Go"
  puts "======================================================"
  if want_to_install?('Go')
    run %{ bash ./install_go.sh }
  end
end

task :install_code_climate do
  puts "======================================================"
  puts "Installing Code Climate CLI...There may be some warnings."
  puts "======================================================"
  if want_to_install?('Code Climate CLI')
    run %{ bash ./install_code_climate.sh }
  end
end

task :install_anycable_go do
  puts "======================================================"
  puts "Installing AnyCable Go"
  puts "======================================================"
  if want_to_install?('AnyCable Go')
    run %{ bash ./install_anycable_go.sh }
  end
end

task :install_overmind do
  puts "======================================================"
  puts "Installing Overmind"
  puts "======================================================"
  if want_to_install?('Overmind')
    run %{ bash ./install_overmind.sh }
  end
end

task :install_deb_get do
  puts "======================================================"
  puts "Installing deb-get"
  puts "======================================================"
  if want_to_install?('deb-get')
    run %{ curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | sudo -E bash -s install deb-get }
    run %{ deb-get install du-dust }
  end
end

task :install_bat do
  puts "======================================================"
  puts "Installing Bat"
  puts "======================================================"
  if want_to_install?('Bat')
    run %{ bash ./install_bat.sh }
  end
end

task :install_popeye do
  puts "======================================================"
  puts "Installing Popeye"
  puts "======================================================"
  if want_to_install?('Popeye')
    run %{ bash ./install_popeye.sh }
  end
end

task :install_helm do
  puts "======================================================"
  puts "Installing Helm"
  puts "======================================================"
  if want_to_install?('Helm')
    run %{ bash ./install_helm.sh }
  end
end

task :install_terraform do
  puts "======================================================"
  puts "Installing Terraform"
  puts "======================================================"
  if want_to_install?('Terraform')
    run %{ bash ./install_terraform.sh }
  end
end

task default: 'install'

private
def run(cmd)
  puts "[Running] #{cmd}"
  `#{cmd}` unless ENV['DEBUG']
end

def number_of_cores
  if $is_macos
    cores = run %{ sysctl -n hw.ncpu }
  else
    cores = run %{ nproc }
  end
  puts
  cores.to_i
end

def run_bundle_config
  return unless system("which bundle")

  bundler_jobs = number_of_cores - 1
  puts "======================================================"
  puts "Configuring Bundlers for parallel gem installation"
  puts "======================================================"
  run %{ . $HOME/.rvm/scripts/rvm && bundle config --global jobs #{bundler_jobs} }
  puts
end

def install_linux_libraries
  # puts "======================================================"
  # puts "Install Kubuntu backport"
  # puts "======================================================"
  # if want_to_install?('Kubuntu backport')
  #   run %{ bash ./install_ppa.sh }
  # end

  puts "======================================================"
  puts "Update and upgrade apt and Tuning"
  puts "======================================================"
  run %{ sudo apt-add-repository -y ppa:neovim-ppa/unstable }
  run %{ sudo apt-add-repository -y ppa:aos1/diff-so-fancy }
  run %{ sudo apt-get update }

  puts "======================================================"
  puts "Optimize swap and cache"
  puts "======================================================"
  if want_to_install?('Tuning')
    run %{ echo "# Decrease swap usage to a more reasonable level" | sudo tee -a /etc/sysctl.conf}
    run %{ echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.conf}
    run %{ echo "# Improve cache management" | sudo tee -a /etc/sysctl.conf}
    run %{ echo "vm.vfs_cache_pressure=50" | sudo tee -a /etc/sysctl.conf}
    run %{ echo "# Increasing the amount of inotify watchers" | sudo tee -a /etc/sysctl.conf}
    run %{ echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf && sudo sysctl -p}
  end

  # puts "======================================================"
  # puts "Allow 3 TTY session, disable kinit, disable apport, remove apport and print-sharing service, disable indexing"
  # puts "======================================================"
  # if want_to_install?('Removing unnecessary services')
  #   run %{ bash ./kill_tty.sh }
  #   run %{ bash ./kill_services.sh }
  #   run %{ ln -nfs "$HOME/.yadr/gtkrc-2.0" "$HOME/.gtkrc-2.0" }
  #   run %{ sudo apt-get purge -y apt-xapian-index }
  # end

  # puts "======================================================"
  # puts "Remove unused libraries and plugins"
  # puts "======================================================"
  # if want_to_install?('Remove unused libraries')
  #   run %{ for package in brltty gnome-orca ttf-indic-fonts-core ttf-punjabi-fonts unity-lens-friends unity-lens-gwibber unity-lens-help unity-lens-music unity-lens-photos unity-lens-radios unity-lens-shopping unity-lens-video; do sudo apt-get -y purge $package; done }
  # end

  puts "======================================================"
  puts "Install useful libraries and plugins"
  puts "======================================================"
  if want_to_install?('Libraries')
    # run %{ sudo apt-get install -y imagemagick --fix-missing }
    # run %{ sudo apt-get install -y libmagickwand-dev }
    # run %{ sudo apt-get install -y ack-grep gpg2 }

    run %{ sudo apt-get install -y zsh exuberant-ctags }
    run %{ sudo apt-get install -y neovim python2 python2-dev python3-dev python3-pip }
    run %{ pip3 install --user pynvim neovim } # For NeoVim plugins
    run %{ gem install neovim } # For NeoVim plugins
    run %{ sudo apt-get install -y ca-certificates apt-transport-https }
    run %{ sudo apt-get install -y libxtst6 xclip tig dos2unix tree }
    run %{ sudo apt-get install -y -f parallel preload cabextract }
    run %{ sudo apt-get install -y -f gdebi-core }
    run %{ sudo apt-get install -f -y }

    puts "======================================================"
    puts "Install silversearch (Ag), ripgrep (rg), fzf and diff-so-fancy"
    puts "======================================================"
    run %{ sudo apt-get install -y -f silversearcher-ag }
    run %{ sudo apt install -y ripgrep diff-so-fancy }

    if want_to_install?('htop & ctop')
      run %{ sudo apt-get install -y htop }
      run %{ sudo chmod 6555 /usr/bin/htop }
      run %{ sudo chown root /usr/bin/htop }
      run %{ sudo wget https://github.com/bcicen/ctop/releases/download/v0.7.7/ctop-0.7.7-linux-amd64 -O /usr/local/bin/ctop }
      run %{ sudo chmod +x /usr/local/bin/ctop }
    end

    if want_to_install?('resilio sync')
      run %{ echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | sudo tee /etc/apt/sources.list.d/resilio-sync.list }
      run %{ wget -qO- https://linux-packages.resilio.com/resilio-sync/key.asc | sudo tee /etc/apt/trusted.gpg.d/resilio-sync.asc > /dev/null 2>&1 }
      run %{ sudo apt-get update }
      run %{ sudo apt-get install resilio-sync }
    end

    # if want_to_install?('Linux Enhancements')
    #   if want_to_install?('TLP')
    #     run %{ sudo add-apt-repository -y ppa:linrunner/tlp }
    #     run %{ sudo apt-get update }
    #     run %{ sudo apt-get install -y tlp tlp-rdw cpufrequtils indicator-cpufreq }
    #     run %{ sudo apt-get install -f -y }
    #     run %{ sudo tlp start }
    #   end

    #   if want_to_install?('tmate')
    #     run %{ sudo add-apt-repository -y ppa:tmate.io/archive }
    #     run %{ sudo apt-get update }
    #     run %{ sudo apt-get install -y tmate}
    #   end

    #   if want_to_install?('exfat')
    #     run %{ sudo apt-get install -y exfat-fuse exfat-utils samba }
    #     run %{ sudo apt-get install -f -y }
    #   end
    # end

    run %{ bash ./install_tmux.sh }
    run %{ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm }
  end

  puts "======================================================"
  puts "Install redis"
  puts "======================================================"
  if want_to_install?('redis')
    run %{ bash ./install_redis.sh }
  end

  puts "======================================================"
  puts "Install python3.11"
  puts "======================================================"
  if want_to_install?('python3.11')
    run %{ sudo add-apt-repository ppa:deadsnakes/ppa -y }
    run %{ sudo apt-get install python3.11 -y }
    # run %{ bash ./install_python34.sh }
  end

  # puts "======================================================"
  # puts "Install apps"
  # puts "======================================================"
  # if want_to_install?('applications')
  #   run %{ bash ./install_apps.sh }
  # end

  puts "======================================================"
  puts "Install Fish shell-like utils"
  puts "======================================================"
  if want_to_install?('Fish shell-like')
    run %{ git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions }
  end

  puts "======================================================"
  puts "Install PostgreSQL"
  puts "======================================================"
  if want_to_install?('PostgreSQL')
    # run %{ bash ./install_postgres.sh }
    run %{ sudo apt-get install -y postgresql postgresql-contrib }

    run %{ echo "CREATE EXTENSION dblink" | sudo -u postgres psql postgres }
    run %{ echo "CREATE USER $USER WITH PASSWORD '$USER' SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION" | sudo -u postgres psql postgres }

    run %{ sudo apt-get -y install pgcli }
    run %{ ln -nfs "$HOME/.yadr/pgpass" "$HOME/.pgpass" }
  end

  puts "======================================================"
  puts "Install Heroku"
  puts "======================================================"
  if want_to_install?('Heroku')
    run %{ bash ./install_heroku.sh }
  end

end

def install_directory_color_fix
  puts "======================================================"
  puts "Directory color fix"
  puts "======================================================"
  if want_to_install?('Directory color fix')
    run %{ sudo curl https://raw.github.com/seebi/dircolors-solarized/master/dircolors.ansi-dark -Lo ~/.dircolors }
  end
end

def install_ssh_tunnel
  puts "======================================================"
  puts "SSH Tunneling"
  puts "======================================================"
  if want_to_install?('SSH Tunneling')
    run %{ mkdir -p ~/.ssh }
    run %{ ln -nfs "$HOME/.yadr/ssh-config" "$HOME/.ssh/config" }
  end
end

def install_rvm_binstubs
  puts "======================================================"
  puts "Installing RVM Bundler support. Never have to type"
  puts "bundle exec again! Please use bundle --binstubs and RVM"
  puts "will automatically use those bins after cd'ing into dir."
  puts "======================================================"
  run %{ source ~/.rvm/scripts/rvm }
  run %{ chmod +x $rvm_path/hooks/after_cd_bundler }
  puts
end

def install_homebrew
  run %{which brew}
  unless $?.success?
    puts "======================================================"
    puts "Installing Homebrew, the OSX package manager...If it's"
    puts "already installed, this will do nothing."
    puts "======================================================"
    run %{bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"}
  end

  puts
  puts
  puts "======================================================"
  puts "Updating Homebrew."
  puts "======================================================"
  run %{ brew update }
  puts
  puts

  puts "======================================================"
  puts "Installing Homebrew packages...There may be some warnings."
  puts "======================================================"
  run %{brew bundle install}
  puts
  puts

  puts "======================================================"
  puts "Setting htop root permission...There may be some warnings."
  puts "======================================================"
  run %{ sudo chown root:wheel /opt/homebrew/bin/htop }
  run %{ sudo chmod u+s /opt/homebrew/bin/htop }
  puts
  puts

  puts "======================================================"
  puts "Installing neovim...There may be some warnings."
  puts "======================================================"
  run %{pip3 install --user pynvim neovim} # For NeoVim plugins
  # run %{pip install --user pynvim neovim} # For NeoVim plugins
  run %{gem install neovim} # For NeoVim plugins
  puts
  puts

  puts "======================================================"
  puts "Installing tmux packages (TPM)...There may be some warnings."
  puts "======================================================"
  run %{ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm }
  run %{ sudo xcode-select -s /Applications/Xcode.app/Contents/Developer }
  run %{ sudo xcodebuild -license accept }
  puts
  puts

  puts "======================================================"
  puts "Installing Postgres App...There may be some warnings."
  puts "======================================================"
  run %{ sudo mkdir -p /etc/paths.d && echo /Applications/Postgres.app/Contents/Versions/latest/bin | sudo tee /etc/paths.d/postgresapp }
  puts
  puts

  puts "======================================================"
  puts "Installing Redis...There may be some warnings."
  puts "======================================================"
  run %{ brew services start redis }
  run %{ sudo sed -i "" "s/# maxmemory <bytes>/maxmemory 128mb/" "/opt/homebrew/etc/redis.conf" }
  run %{ sudo sed -i "" "s/# maxmemory-policy noeviction/maxmemory-policy allkeys-lru/" "/opt/homebrew/etc/redis.conf" }
  puts
  puts

  puts "======================================================"
  puts "Cleaning Homebrew...There may be some warnings."
  puts "======================================================"
  run %{ brew cleanup }
  puts
  puts
end

def install_fonts
  puts "======================================================"
  puts "Installing patched fonts for Powerline/Lightline."
  puts "======================================================"
  run %{ cp -f $HOME/.yadr/fonts/* $HOME/Library/Fonts } if $is_macos
  run %{ mkdir -p ~/.fonts && cp ~/.yadr/fonts/* ~/.fonts && fc-cache -vf ~/.fonts } if $is_linux
  puts
end

def install_term_theme
  puts "======================================================"
  puts "Installing iTerm2 solarized theme."
  puts "======================================================"
  run %{ /usr/libexec/PlistBuddy -c "Add :'Custom Color Presets':'Solarized Light' dict" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Merge 'iTerm2/Solarized Light.itermcolors' :'Custom Color Presets':'Solarized Light'" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Add :'Custom Color Presets':'Solarized Dark' dict" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Merge 'iTerm2/Solarized Dark.itermcolors' :'Custom Color Presets':'Solarized Dark'" ~/Library/Preferences/com.googlecode.iterm2.plist }

  # If iTerm2 is not installed or has never run, we can't autoinstall the profile since the plist is not there
  if !File.exists?(File.join(ENV['HOME'], '/Library/Preferences/com.googlecode.iterm2.plist'))
    puts "======================================================"
    puts "To make sure your profile is using the solarized theme"
    puts "Please check your settings under:"
    puts "Preferences> Profiles> [your profile]> Colors> Load Preset.."
    puts "======================================================"
    return
  end

  # Ask the user which theme he wants to install
  message = "Which theme would you like to apply to your iTerm2 profile?"
  color_scheme = ask message, iTerm_available_themes

  return if color_scheme == 'None'

  color_scheme_file = File.join('iTerm2', "#{color_scheme}.itermcolors")

  # Ask the user on which profile he wants to install the theme
  profiles = iTerm_profile_list
  message = "I've found #{profiles.size} #{profiles.size>1 ? 'profiles': 'profile'} on your iTerm2 configuration, which one would you like to apply the Solarized theme to?"
  profiles << 'All'
  selected = ask message, profiles

  if selected == 'All'
    (profiles.size-1).times { |idx| apply_theme_to_iterm_profile_idx idx, color_scheme_file }
  else
    apply_theme_to_iterm_profile_idx profiles.index(selected), color_scheme_file
  end
end

def iTerm_available_themes
  Dir['iTerm2/*.itermcolors'].map { |value| File.basename(value, '.itermcolors')} << 'None'
end

def iTerm_profile_list
  profiles=Array.new
  begin
    profiles <<  %x{ /usr/libexec/PlistBuddy -c "Print :'New Bookmarks':#{profiles.size}:Name" ~/Library/Preferences/com.googlecode.iterm2.plist 2>/dev/null}
  end while $?.exitstatus==0
  profiles.pop
  profiles
end

def ask(message, values)
  puts message
  while true
    values.each_with_index { |val, idx| puts " #{idx+1}. #{val}" }
    selection = STDIN.gets.chomp
    if (Float(selection)==nil rescue true) || selection.to_i < 0 || selection.to_i > values.size+1
      puts "ERROR: Invalid selection.\n\n"
    else
      break
    end
  end
  selection = selection.to_i-1
  values[selection]
end

def install_prezto
  puts
  puts "Installing Prezto (ZSH Enhancements)..."

  run %{ ln -nfs "$HOME/.yadr/zsh/prezto" "${ZDOTDIR:-$HOME}/.zprezto" }

  # The prezto runcoms are only going to be installed if zprezto has never been installed
  install_files(Dir.glob('zsh/prezto/runcoms/zlogin'), :symlink)
  install_files(Dir.glob('zsh/prezto/runcoms/zlogout'), :symlink)
  install_files(Dir.glob('zsh/prezto/runcoms/zpreztorc'), :symlink)
  install_files(Dir.glob('zsh/prezto/runcoms/zprofile'), :symlink)
  install_files(Dir.glob('zsh/prezto/runcoms/zshenv'), :symlink)

  puts
  puts "Overriding prezto ~/.zpreztorc with YADR's zpreztorc to enable additional modules..."
  run %{ ln -nfs "$HOME/.yadr/zsh/prezto-override/zpreztorc" "${ZDOTDIR:-$HOME}/.zpreztorc" }
  run %{ ln -s ~/.zprezto/modules/prompt/external/powerlevel9k/powerlevel9k.zsh-theme ~/.zprezto/modules/prompt/functions/prompt_powerlevel9k_setup }

  puts
  puts "Installing docker completion..."
  run %{ curl -fLo ~/.zprezto/modules/completion/external/src/_docker https://raw.githubusercontent.com/docker/cli/master/contrib/completion/zsh/_docker }
  run %{ curl -fLo ~/.zprezto/modules/completion/external/src/_docker-compose https://raw.githubusercontent.com/docker/compose/master/contrib/completion/zsh/_docker-compose }

  puts
  puts "Creating directories for your customizations"
  run %{ mkdir -p $HOME/.zsh.before }
  run %{ mkdir -p $HOME/.zsh.after }
  run %{ mkdir -p $HOME/.zsh.prompts }

  if "#{ENV['SHELL']}".include? 'zsh' then
    puts "Zsh is already configured as your shell of choice. Restart your session to load the new settings"
  else
    puts "Setting zsh as your default shell"
    if File.exists?("/usr/local/bin/zsh")
      if File.readlines("/private/etc/shells").grep("/usr/local/bin/zsh").empty?
        puts "Adding zsh to standard shell list"
        run %{ echo "/usr/local/bin/zsh" | sudo tee -a /private/etc/shells }
      end
      run %{ chsh -s /usr/local/bin/zsh $USER }
    else
      run %{ chsh -s /bin/zsh }
    end
  end
end

def want_to_install? (section)
  if ENV["ASK"]=="true"
    puts "Would you like to install configuration files for: #{section}? [y]es, [n]o"
    STDIN.gets.chomp == 'y'
  else
    true
  end
end

def install_files(files, method = :symlink)
  files.each do |f|
    file = f.split('/').last
    source = "#{ENV["PWD"]}/#{f}"
    target = "#{ENV["HOME"]}/.#{file}"

    puts "======================#{file}=============================="
    puts "Source: #{source}"
    puts "Target: #{target}"

    if File.exists?(target) && (!File.symlink?(target) || (File.symlink?(target) && File.readlink(target) != source))
      puts "[Overwriting] #{target}...leaving original at #{target}.backup..."
      run %{ mv "$HOME/.#{file}" "$HOME/.#{file}.backup" }
    end

    if method == :symlink
      run %{ ln -nfs "#{source}" "#{target}" }
    else
      run %{ cp -f "#{source}" "#{target}" }
    end

    puts "=========================================================="
    puts
  end
end

def list_vim_submodules
  result=`git submodule -q foreach 'echo $name"||"\`git remote -v | awk "END{print \\\\\$2}"\`'`.select{ |line| line =~ /^vim.bundle/ }.map{ |line| line.split('||') }
  Hash[*result.flatten]
end

def apply_theme_to_iterm_profile_idx(index, color_scheme_path)
  values = Array.new
  16.times { |i| values << "Ansi #{i} Color" }
  values << ['Background Color', 'Bold Color', 'Cursor Color', 'Cursor Text Color', 'Foreground Color', 'Selected Text Color', 'Selection Color']
  values.flatten.each { |entry| run %{ /usr/libexec/PlistBuddy -c "Delete :'New Bookmarks':#{index}:'#{entry}'" ~/Library/Preferences/com.googlecode.iterm2.plist } }

  run %{ /usr/libexec/PlistBuddy -c "Merge '#{color_scheme_path}' :'New Bookmarks':#{index}" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ defaults read com.googlecode.iterm2 }
end

def success_msg(action)
  puts %q{
   _     _           _
  | |   | |         | |
  | |___| |_____  __| | ____
  |_____  (____ |/ _  |/ ___)
   _____| / ___ ( (_| | |
  (_______\_____|\____|_|
  }
  puts "YADR has been #{action}. Please restart your terminal and vim."
end
