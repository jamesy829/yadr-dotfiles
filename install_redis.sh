#!/bin/bash --login

OS_CODENAME=`lsb_release -cs`

if [ "$OS_CODENAME" = "bionic" ]; then
  sudo apt-get install -y redis
else
  sudo apt-get install -y redis-server
fi

sudo sed -i "s/# maxmemory <bytes>/maxmemory 128mb/" "/etc/redis/redis.conf"
sudo sed -i "s/# maxmemory-policy noeviction/maxmemory-policy allkeys-lru/" "/etc/redis/redis.conf"
