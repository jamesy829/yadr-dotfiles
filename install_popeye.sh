#!/bin/bash --login

cd ~
pwd

mkdir popeye
cd popeye
wget https://github.com/derailed/popeye/releases/download/v0.11.2/popeye_Linux_amd64.tar.gz
tar -xzf popeye_Linux_amd64.tar.gz
sudo mv popeye /usr/local/bin
rm popeye_Linux_amd64.tar.gz

cd ..
rm -r popeye
