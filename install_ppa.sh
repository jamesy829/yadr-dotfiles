#!/bin/bash --login

OS_CODENAME=`lsb_release -cs`

if [ "$OS_CODENAME" = "xenial" ]; then
  sudo apt-add-repository -y ppa:kubuntu-ppa/backports

  sudo apt-get update
  sudo apt-get full-upgrade -y
fi
