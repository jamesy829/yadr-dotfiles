" Codeium's default keybinding
let g:codeium_disable_bindings = 0

" imap <script><silent><nowait><expr> <C-g> codeium#Accept()
" imap <C-;>   <Cmd>call codeium#CycleCompletions(1)<CR>
" imap <C-,>   <Cmd>call codeium#CycleCompletions(-1)<CR>
" imap <C-x>   <Cmd>call codeium#Clear()<CR>
