set relativenumber number

set diffopt+=vertical
set clipboard+=unnamed
set list listchars=tab:\ \ ,trail:·

set gcr=a:blinkon0  "Disable cursor blink
set visualbell      "No sounds

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowritebackup
set nowb

" ================ Persistent Undo ==================

" Keep undo history across sessions, by storing in file.
" Only works all the time.
set undofile

" ================ Indentation ======================

set smartindent
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
" Some file types use real tabs
au FileType {make,gitconfig} set noexpandtab sw=2

" Auto indent pasted text
nnoremap p p=`]<C-o>
nnoremap P P=`]<C-o>

set nowrap    "Don't wrap lines
set linebreak "Wrap lines at convenient points

" ================ Folds ============================

set foldenable
set foldmethod=manual "fold based on indent
set foldlevelstart=10 "Open most of the folds by default. If set to 0, all folds will be closed.
set foldnestmax=10 "Folds can be nested. Setting a max value protects you from too many folds.

" ================ Completion =======================

set wildmode=list:longest
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif

" ================ Scrolling ========================

set scrolloff=6 "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

" ================ Search ===========================

set ignorecase " Ignore case when searching...
set smartcase  " ...unless we type a capital

" ================ Formatting =======================
set formatoptions+=j " Delete comment character when joining commented lines

" Better display for messages
set cmdheight=2
" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" Resize windows depending on context
set winwidth=88
set winheight=10
set winminheight=10
set winheight=999

" ================ Functions =======================
function! GetProjectName()
  return fnamemodify(getcwd(), ':t')
endfunction

function! Autowrite()
  for tabnum in range(tabpagenr('$'))
    for bufnum in tabpagebuflist(tabnum + 1)
      if bufname(bufnum) =~ '^quickfix-' | return | endif
    endfor
  endfor
  silent! wa
  if &modified
    silent! GutentagsUpdate
  endif
endfunction

function! DetectBinaryFile()
  if &filetype == ''
    \ && expand('%') !~ '\.\(bz2\|gz\|lzma\|xz\|Z\)$'
    \ && !!search('\%u0000', 'wn')
    Hexmode
  endif
endfunction

function! SetTmuxWindowName()
  let cmd = 'rename=$(tmux show-window-options -t $TMUX_PANE -v automatic-rename);'
  let cmd .= 'if [[ $rename != "off" ]]; then;'
  let cmd .= "tmux rename-window -t $TMUX_PANE '" . GetProjectName() . "';"
  let cmd .= 'fi'
  call jobstart(cmd)
endfunction

function! RestoreTmuxWindowName()
  let cmd = 'number_of_vims=$(tmux list-panes -F "#{pane_current_command}" | grep -c vim);'
  let cmd .= 'if [[ $number_of_vims -eq 1 ]]; then;'
  let cmd .= 'tmux setw automatic-rename;'
  let cmd .= 'fi'
  call system(cmd)
endfunction

" ================ Autocommands =======================
augroup improved_autowrite
  autocmd!
  autocmd FocusLost,BufLeave * call Autowrite()
augroup end

augroup improved_autoread
  autocmd!
  autocmd FocusGained,BufEnter * silent! checktime
augroup end

augroup auto_mkdir
  autocmd!
  autocmd BufWritePre *
    \ if !isdirectory(expand('<afile>:p:h')) |
    \   call mkdir(expand('<afile>:p:h'), 'p') |
    \ endif
augroup end

augroup detect_filetypes
  autocmd!
  autocmd BufRead,BufNewFile *spec.rb set ft=ruby.rspec
  autocmd BufRead,BufNewFile *.html.erb set ft=eruby.html
  autocmd BufRead,BufNewFile *.js.erb set ft=eruby.javascript
  autocmd BufRead,BufNewFile *.nfo,*.NFO set ft=nfo
  autocmd BufRead,BufNewFile *.jsx,*.js.es6 set ft=javascript
  autocmd BufRead,BufNewFile *.js.es6.erb set ft=eruby.javascript
  autocmd BufRead,BufNewFile *.env*,Procfile*,*.config set ft=conf
  autocmd BufRead,BufNewFile *.apib set ft=markdown
  autocmd BufRead,BufNewFile *.vue setfiletype html
  autocmd BufRead,BufNewFile Rakefile,Brewfile set ft=ruby
  autocmd BufRead,BufNewFile Dockerfile* set ft=dockerfile
augroup end

augroup detect_binary_files
  autocmd!
  autocmd BufRead * call DetectBinaryFile()
augroup end

if exists('$TMUX') && has('nvim')
  augroup tmux_window_name
    autocmd!
    autocmd VimEnter * call SetTmuxWindowName()
    autocmd VimLeave * call RestoreTmuxWindowName()
  augroup end
endif
