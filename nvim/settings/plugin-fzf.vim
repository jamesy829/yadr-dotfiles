nnoremap <Leader>t :BTags<CR>
nnoremap <Leader>T :Tags<CR>

nnoremap <silent> ,a :Files<CR>
nnoremap <silent> ,g :GFiles<CR>
nnoremap <C-b> :Buffers<CR>
nnoremap <C-f> :Rg<space>
