#!/bin/bash --login

OS_CODENAME=`lsb_release -cs`

sudo add-apt-repository ppa:pi-rho/dev -y
sudo apt-get update
sudo apt-get install tmux -y
