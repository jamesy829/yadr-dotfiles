#!/bin/bash --login

cd ~
pwd

# Install Node
export node_version="22.3.0"

export NVM_DIR="$HOME/.nvm"

# Load OS X specific config
if [[ "$OSTYPE" == darwin* ]]; then
  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && . "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
fi

# Load Linux specific config
if [[ "$OSTYPE" == linux* ]]; then
  curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

  echo -e "\e[36mDone installing NVM sourcing...\e[0m"
  echo "source $HOME/.nvm/nvm.sh" >> $HOME/.profile
  source $NVM_DIR/nvm.sh
  chown -R $USER:$USER $NVM_DIR
fi

source ~/.profile
source ~/.zshrc

echo -e "\e[36mDefining default global packages\e[0m"
touch $NVM_DIR/default-packages
echo "eslint" | sudo tee -a $NVM_DIR/default-packages
echo "eslint_d" | sudo tee -a $NVM_DIR/default-packages
echo "@babel/eslint-parser" | sudo tee -a $NVM_DIR/default-packages
echo "@babel/preset-react" | sudo tee -a $NVM_DIR/default-packages
echo "eslint-plugin-react" | sudo tee -a $NVM_DIR/default-packages
echo "eslint-plugin-vue" | sudo tee -a $NVM_DIR/default-packages
echo "prettier" | sudo tee -a $NVM_DIR/default-packages
echo "typescript" | sudo tee -a $NVM_DIR/default-packages
echo "neovim" | sudo tee -a $NVM_DIR/default-packages

echo -e "\e[36mInstalling node $node_version\e[0m"
nvm install $node_version
nvm alias default $node_version
nvm use $node_version
