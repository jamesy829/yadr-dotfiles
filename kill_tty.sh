#!/bin/bash --login

if [ "`lsb_release -cs`" == "xenial" ]; then
  echo "TTY does not need to be modified"
else
  if [[ -f /etc/default/console-setup ]]; then
    ( sudo sed -i -r 's~^ACTIVE_CONSOLES="/dev/tty[[]1-[0-9]+[]]"~ACTIVE_CONSOLES="/dev/tty[1-3]"~' /etc/default/console-setup )

    for tty in /etc/init/tty*.conf; do
      if [[ ${tty:13:1} -gt 3 ]]; then
        echo -e "\e[1;33mI'm removing the TTY '$tty'.\e[0m" && ( sudo rm $tty )
      fi
    done
  fi
fi
