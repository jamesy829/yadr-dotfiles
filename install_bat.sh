#!/bin/bash --login

cd ~
pwd

wget https://github.com/sharkdp/bat/releases/download/v0.23.0/bat_0.23.0_amd64.deb
sudo dpkg -i bat_0.23.0_amd64.deb
rm bat_0.23.0_amd64.deb
