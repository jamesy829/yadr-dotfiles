#!/bin/bash --login

wget https://github.com/meetfranz/franz/releases/download/v5.1.0/franz_5.1.0_amd64.deb
sudo dpkg -i --force-depends franz_5.1.0_amd64.deb
sudo apt-get install -f
sudo dpkg -i --force-depends franz_5.1.0_amd64.deb
rm franz_5.1.0_amd64.deb

wget https://github.com/edipox/wunderlistux/releases/download/Linux-0.0.9/Wunderlistux_0.0.9_amd64.deb
sudo dpkg -i --force-depends Wunderlistux_0.0.9_amd64.deb
sudo apt-get install -f
sudo dpkg -i --force-depends Wunderlistux_0.0.9_amd64.deb
rm Wunderlistux_0.0.9_amd64.deb

wget https://github.com/Automattic/simplenote-electron/releases/download/v1.6.0/Simplenote-linux-1.5.0-amd64.deb
sudo dpkg -i --force-depends Simplenote-linux-1.6.0-amd64.deb
sudo apt-get install -f
sudo dpkg -i --force-depends Simplenote-linux-1.6.0-amd64.deb
rm Simplenote-linux-1.6.0-amd64.deb

sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/Release.key
sudo apt-key add Release.key
rm Release.key
sudo apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/ -y
sudo apt-get update
sudo apt-get install --install-recommends winehq-staging -y
sudo apt-get install -f
sudo apt-get install --install-recommends winehq-staging -y

wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
sudo mv winetricks /usr/local/bin

wget -O YNAB4_LinuxInstall.pl https://raw.github.com/WolverineFan/YNABLinuxInstall/master/YNAB4_LinuxInstall.pl
perl ./YNAB4_LinuxInstall.pl
env LIBGL_ALWAYS_SOFTWARE=1 WINEPREFIX="/home/james/.wine_YNAB4" wine C:\\Program\ Files\ \(x86\)\\YNAB\ 4\\YNAB\ 4.exe

wget https://scdn.line-apps.com/client/win/new/LineInst.exe
WINEPREFIX="$HOME/.wine_line" wine LineInst.exe
WINEPREFIX="$HOME/.wine_line" wine "C:\\users\\james\\Local Settings\\Application Data\\Line\\bin\\LineLauncher.exe"

sudo apt-get update
sudo apt-get install fcitx fcitx-chewing kde-config-fcitx -y
im-config -n fcitx
