#!/bin/bash --login

cd ~
pwd

curl -L https://github.com/codeclimate/codeclimate/archive/master.tar.gz | tar xvz
cd codeclimate-* && sudo make install
rm -r codeclimate-*
