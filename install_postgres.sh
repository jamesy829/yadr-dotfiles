#!/bin/bash --login

echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee -a /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-11 postgresql-client-11 postgresql-contrib-11 libpq-dev libgmp3-dev postgresql-server-dev-11

PG_VERSION=${1:-"11"}

PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"
OS_CODENAME=`lsb_release -cs`

# Edit postgresql.conf to listen on all addresses (not just localhost)
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

# Append to pg_hba.conf to add password auth:
echo "host    all             all             all                   md5" | sudo tee -a "$PG_HBA"

# Explicitly set default client_encoding
echo "client_encoding = utf8" | sudo tee -a "$PG_CONF"
echo "max_connections = 200" | sudo tee -a "$PG_CONF"
echo "shared_buffers = 512MB" | sudo tee -a "$PG_CONF"
echo "effective_cache_size = 1536MB" | sudo tee -a "$PG_CONF"
echo "work_mem = 2621kB" | sudo tee -a "$PG_CONF"
echo "maintenance_work_mem = 128MB" | sudo tee -a "$PG_CONF"
if [ "$PG_VERSION" == "9.4" ]; then
  echo "checkpoint_segments = 32" | sudo tee -a "$PG_CONF"
else
  echo "min_wal_size = 1GB" | sudo tee -a "$PG_CONF"
  echo "max_wal_size = 2GB" | sudo tee -a "$PG_CONF"
fi
echo "checkpoint_completion_target = 0.7" | sudo tee -a "$PG_CONF"
echo "wal_buffers = 16MB" | sudo tee -a "$PG_CONF"
echo "default_statistics_target = 100" | sudo tee -a "$PG_CONF"

echo "shared_preload_libraries = 'pg_stat_statements'" | sudo tee -a "$PG_CONF"
echo "pg_stat_statements.track = all" | sudo tee -a "$PG_CONF"

# Restart so that all new config is loaded

if [ "$OS_CODENAME" = "xenial" ]; then
  sudo systemctl restart postgresql.service
else
  sudo /etc/init.d/postgresql restart
fi

# install dblink exstension
echo "CREATE EXTENSION dblink" | sudo -u postgres psql postgres
echo "CREATE USER $USER WITH PASSWORD '$USER' SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION" | sudo -u postgres psql postgres

sudo apt-get install pgcli

sudo apt-get autoremove -y
sudo apt-get autoclean -y
