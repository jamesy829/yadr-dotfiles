#!/bin/bash --login

cd ~
pwd

wget https://github.com/anycable/anycable-go/releases/download/v1.0.2/anycable-go-linux-amd64
mv anycable-go-linux-amd64 anycable-go
chmod +x anycable-go
sudo mv anycable-go /usr/local/bin
