#!/bin/bash --login

OS_CODENAME=`lsb_release -cs`

if [ "$OS_CODENAME" = "xenial" ]; then
  sudo apt-get install -y build-essential checkinstall
  sudo apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
  sudo apt-get install -y libssl-dev openssl
  sudo apt-get install -y lzma lzma-dev liblzma-dev

  wget https://www.python.org/ftp/python/3.12.1/Python-3.12.1.tgz
  sudo tar xzf Python-3.12.1.tgz
  sudo rm -rf Python-3.12.1.tgz
  cd Python-3.12.1
  # sudo ./configure --prefix=/opt/python3.4 --with-zlib-dir=/usr/local/lib/ --with-ensurepip=install
  sudo ./configure --prefix=/opt/python3.4 --with-ensurepip=install
  sudo make altinstall
  cd ..
  sudo rm -rf Python-3.12.1
  sudo ln -nfs /opt/python3.4/bin/python3.4 /usr/bin/python3.4
  sudo ln -nfs /opt/python3.4/bin/pip3.4 /usr/bin/pip
else
  echo "Not installing python3.4 on non-xenial release"
fi
