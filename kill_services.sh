#!/bin/bash --login

if [[ -f /etc/initramfs-tools/conf.d/resume ]]; then
  echo -e "\e[1;33mI'm disabling 'kinit'.\e[0m" && ( sudo sed -i -r 's~^#?RESUME=~#RESUME=~' /etc/initramfs-tools/conf.d/resume )
fi

if [[ -f /etc/default/apport ]]; then
  echo -e "\e[1;33mI'm disabling 'apport'.\e[0m" && ( sudo sed -i -r 's~^enabled=1$~enabled=0~' /etc/default/apport )
fi

for service in apport bluetooth saned; do
  echo -e "\e[1;33mI'm removing the '$service' service.\e[0m" && ( sudo update-rc.d -f $service remove ) > /dev/null
done
