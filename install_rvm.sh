#!/bin/bash --login

cd ~
pwd

# Load OS X specific config
if [[ "$OSTYPE" == darwin* ]]; then
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  curl -sSL https://get.rvm.io | bash -s stable
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
  source ~/.zshrc

  echo "jazz_fingers" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "amazing_print" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "nokogiri" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "rubocop" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "neovim" | sudo tee -a ~/.rvm/gemsets/global.gems

  rvm reinstall 3.3.3 --with-openssl-dir=$(brew --prefix openssl) --with-readline-dir=$(brew --prefix readline) --with-libyaml-dir=$(brew --prefix libyaml) --disable-dtrace --disable-docs
fi

# Load Linux specific config
if [[ "$OSTYPE" == linux* ]]; then
  sudo apt-get update

  sudo apt-get install -y curl g++ gcc autoconf automake bison libc6-dev \
        libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool \
        libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev \
        libreadline-dev libssl-dev
  gpg --keyserver keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  curl -sSL https://get.rvm.io | bash -s stable

  echo "amazing_print" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "nokogiri" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "rubocop" | sudo tee -a ~/.rvm/gemsets/global.gems
  echo "neovim" | sudo tee -a ~/.rvm/gemsets/global.gems

  source ~/.rvm/scripts/rvm
  source ~/.zshrc

  rvm install ruby-3.3.3
fi

