#!/bin/bash --login

cd ~
pwd

curl -O https://dl.google.com/go/go1.22.3.linux-amd64.tar.gz
sudo tar -xzf go1.22.3.linux-amd64.tar.gz
sudo mv go /usr/local
rm go1.22.3.linux-amd64.tar.gz
